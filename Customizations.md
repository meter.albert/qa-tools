# Aliases used for git

1. `alias gaa="git add ."`
1. `alias gst="git status"`
1. `alias gbr="git branch"`
1. `alias gch="git checkout"`
1. `alias gco="git commit -m"`

# Custom Vim additions
1. Enable easymotion plugin in Vim settings
1.
    ``` 
    "vim.insertModeKeyBindings": [
        {
            "before": ["j", "j"],
            "after": ["<Esc>"]
        }
    ]
    ``` 
    This adds the ability to use double j as a way to go back to normal/command mode. this needs to be added to the settings.json file
1. Change `Easymotion Marker Background Color` to `#ff0000` (red)
1. Change `Easymotion Marker Foreground Color` (there are two values) to `#fff` (white)