# QA Tools

The goal here is to create tools to help with testing.

# Planned Projects/Updates

1. Text parser and sorter for analytics

* There's an issue with certain wireshark events, because the request uri is included in the stuff that's being copied and pasted. Need to check for duplicates and remove them. I saw this on Heartbeats for Chromecast events, but I'll need to see if this happens on analytics events as well

* I want to add a sorting function that works on the evar and props in heartbeats. I think I can .pop() these items out and then put them in a different div container

* Need support for + signs in titles for analytics and heartbeats

* There's a bug with the heartbeat regex where the key is missing the last letter. Example data:

    ```
    l:event:playhead	0
    l:event:duration	7510
    s:event:type	play
    s:asset:name	Eight Days that Made Rome: Hannibal's Last Stand
    s:asset:type	main
    l:asset:length	2609
    ```

2. Runtime Calculator

* Add option to paste in cuepoint data. Will need to account for possible quotations and commas 

3. Adding some cleanup to the UVP functions

* not sure if this is still needed after switching to a table format

# Setup

1. `git init`
1. `git pull https://gitlab.com/meter.albert/qa-tools.git`
1. `git remote add origin https://gitlab.com/meter.albert/qa-tools.git`
1. I think at this point, `push` will work properly, assuming changes are tracked and commited. 

# Setting Up a New Branch

Note: don't include the <> brackets in the code below

1. `git checkout -b <name of new local branch>`
1. `git branch --set-upstream-to=origin/<name of remote branch> <name of local branch>` This will set the branch that you push to/pull from when you're working in this branch locally

# Forcing a pull (overwriting local files)

1. `git fetch --all` This downloads the latest from the remote, but doesn't merge the changes
1. `git reset --hard origin/<branch_name>` This merges the changes into the `<branch_name>`, overwriting local files

# Merging remote (and local) branches without a merge request

1. `git checkout <name of destination remote branch> ` 
1. `git pull origin <name of source remote branch>` This overwrites the local files with the remote source branch
1. `git push` This pushes the overwritten local files to the remote destination branch