# UVP Notes

## Event types
### sessionStart
* Should include most of the params 
* Notable exceptions:
    * any of the `params.media.ad` items 
* Known issues/Missing Items:
    * `a.media.season`, `a.media.episode`
    * Missing `s.prop/eVar23` on content without ads
    * Missing `s.prop/eVar11` (castState), `s.prop/eVar20` (thumbnailNewBadging), `s.prop/eVar38` (itemTitle)

### adBreakStart
* `media.ad.podFriendlyName`
* `media.ad.podIndex`
* `media.ad.podSecond`

### adStart
* `media.ad.length`
* `media.ad.playerName`
* `media.ad.name`
* `media.ad.podPosition`
* `media.ad.id`

### play, ping, sessionComplete, adBreakComplete, adComplete
* `playerTime.ts`
* `playerTime.playhead`
### adStart