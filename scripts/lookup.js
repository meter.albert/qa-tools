window.onload = () => {
	let addHandlers = (button, input, func) => {
		let idButton = document.getElementById(button);
		if (input) {
			let idInput = document.getElementById(input);
			enableEnter(idButton, idInput);
		}
		idButton.addEventListener('click', (e) => {
			func();
		});
	};

	let enableEnter = (idButton, idInput) => {
		idInput.addEventListener('keyup', (e) => {
			if (e.keyCode === 13) {
				// this is the keyCode for the Enter button
				// Cancel the default action, if needed
				e.preventDefault();
				// Trigger the button element with a click
				idButton.click();
			}
		});
	};

	addHandlers('episodeIDButton', 'episodeIDInput', fillEpisodeDisplay);
	addHandlers('showIDButton', 'showIDInput', fillShowDisplay);
	addHandlers('seriesIDButton', 'seriesIDInput', fillSeriesDisplay);
	addHandlers('unpubButton', null, fillUnpubDisplay);
};

let fillEpisodeDisplay = () => {
	let episodeDisplay = document.getElementById('episodeDisplay');
	let contentType = 'episode';
	let episodeIDInput = document.getElementById('episodeIDInput').value;
	episodeDisplay.innerHTML = `${makeLink(contentType, episodeIDInput)}`;
	let h = document.querySelector('#lookupContent > h1');
	h.innerHTML = 'Opening ' + h.innerHTML;
};

let fillShowDisplay = () => {
	let showDisplay = document.getElementById('showDisplay');
	let contentType = 'show';
	let showIDInput = document.getElementById('showIDInput').value;
	let objResult = allShows.find((show) => {
		return show.showId == showIDInput;
	});
	let slug = objResult.slug;
	showDisplay.innerHTML = `${makeLink(contentType, slug)}`;
	let h = document.querySelector('#lookupContent > h1');
	h.innerHTML = 'Opened ' + h.innerHTML;
};

let fillSeriesDisplay = () => {
	let seriesDisplay = document.getElementById('seriesDisplay');
	let contentType = 'series';
	let seriesIDInput = document.getElementById('seriesIDInput').value;
	let objResult = allSeries.find((series) => {
		return series.seriesId == seriesIDInput;
	});
	let slug = objResult.slug;
	seriesDisplay.innerHTML = `${makeLink(contentType, slug)}`;
	let h = document.querySelector('#lookupContent > h1');
	h.innerHTML = 'Open ' + h.innerHTML;
};

let fillUnpubDisplay = () => {
	let unpubDisplay = document.getElementById('unpubDisplay');
	unpubDisplay.innerHTML = unpubList();
};

let makeLink = (contentType, slugOrID) => {
	let base = 'https://scotty-cms-qa.smithsonianearthtv.com/';
	let url = `${base}${contentType}/${slugOrID}`;
	return `<a href="${url}" target="_blank">${url}</a>`;
};

let unpubList = () => {
	// list of all the published series
	let published = allSeries.filter((series) => {
		if (series.published == true) {
			// console.log(series.title);
			return series;
		}
	});
	// list of the unpublished episodes of published series
	let list = [];
	published.map((series) => {
		return series.seasons.map((season) => {
			season.episodes.map((episode) => {
				if (episode.published == true) {
					list.push(episode);
				}
			});
		});
	});
	console.log(list);
	let unpubVideo = [];
	list.forEach((episode) => {
		episode.videos.forEach((video) => {
			if (video.published == false) {
				unpubVideo.push(`${episode.title} - ${video.title}`);
			}
		});
	});

	return unpubVideo.join('<br>');
};
