let matchPatterns = [
	'event',
	'eVar',
	'prop',
	'length',
	'playerName',
	'friendlyName',
	'name',
	'show',
	'season',
	'episode',
	'video_id',
	'genre',
	'feed'
];

let dqs = (selector) => {return document.querySelector(selector)} // I'm lazy :)

// Helper for adding event listeners
let addEL = (element, e, func, args) => {
        element.addEventListener(e, () => {
            func(args) // will use an array for args if I need them
        })
    }

// Stuff that runs on DOM load. Listeners and div creation for formatted output
document.addEventListener('DOMContentLoaded', function(event) {
	addEL(dqs('#analyticsButton'), 'click', pullAnalytics);
	addEL(dqs('#heartbeatsButton'), 'click', pullHeartbeats);
	addEL(dqs('#clearButton'), 'click', clearBoxes);
	
	// Makes new divs for each item in matchPattern
	matchPatterns.forEach((match) => {
		temp = document.createElement('div'); // makes a div
		temp.className = match + ' spacy'; // gives it a class
		document.getElementById('analyticsContent').appendChild(temp); // inserts new div into DOM
	});
});

let eVarPropSort = (a, b) => {
	// sorting function for eVars and props
	let indA = a.match(RegExp(dataJson.metadataRegex))[1]; // match method returns an array, and the [1] index is the first capturing group
	let indB = b.match(RegExp(dataJson.metadataRegex))[1];
	return indA - indB;
};

let pullHeartbeats = () => {
	// console.log(`heartbeat button was pushed`);
	let data = document.myform.databox.value; // grabs from databox
	let filtered = data.match(RegExp(dataJson.heartbeatRegex, 'g')); // uses black magic to identify metadata key and value pairs and makes an array of them
	let formattedList = [];

	filtered.forEach((line) => {
		let formatted = line.match(RegExp(dataJson.heartbeatRegex));
		// the match method returns the full match at index 0, then the parts I need at 1, 2, 3 and 4
		formattedList.push(
			formatted[1] + ':' + '<b>' + formatted[2] + '</b>' + ' = ' + formatted[3]
			// + ' = ' + decodeURIComponent(formatted[4])
		);
	});
	formattedList.sort();
	dqs('#heartbeat').innerHTML = formattedList.join('<br>');
	return false;
};

let objStorage = {}; // this sits out here so the clearBoxes function can use it

let pullAnalytics = () => {
	let data = document.myform.databox.value;
	let line = data.split('\n');

	matchPatterns.forEach((match) => {
		objStorage[match] = []; // generates a key for each match and sets it equal to an empty array
		line.forEach((item) => {
			if (item.includes(match)) {
				objStorage[match].push(decodeURIComponent(item)); // decoded item gets pushed into array if it includes match
			}
		});
	});

	// channel and pageName need special handling because they can appear multiple times, and we need them in a specific position
	let channel = data.match(RegExp(dataJson.channelRegex));
	let pageName = data.match(RegExp(dataJson.pageNameRegex));
	dqs('#channel').innerHTML = `${channel[1]} ${channel[2]}`;
	dqs('#pageName').innerHTML = `${pageName[1]} ${pageName[2]}`;

	Object.keys(objStorage).forEach((metaVariables) => {
		if (
			objStorage[metaVariables].length > 1 &&
			(metaVariables.includes('eVar') || metaVariables.includes('prop'))
		) {
			// only eVars and props need the special sorting function
			objStorage[metaVariables].sort(eVarPropSort);
		}
		temp = document.getElementsByClassName(metaVariables)[0];
		temp.innerHTML = objStorage[metaVariables].join('<br>'); // divs with class of metaVariables were already generated
	});

	return false;
};

let clearBoxes = () => {
	document.getElementById('heartbeat').innerHTML = '';

	matchPatterns.forEach((match) => {
		document.getElementsByClassName(match)[0].innerHTML = '';
	});
	document.getElementById('channel').innerHTML = ``;
	document.getElementById('pageName').innerHTML = ``;
	document.getElementsByName('databox').innerHTML = ``;
};
// let paste = async () => {
// 	const text = await navigator.clipboard.readText(); // Reads text from clipboard
// 	console.log(text);
// };
