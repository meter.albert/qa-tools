// Stuff that runs on DOM load. Listeners and div creation for formatted output
document.addEventListener('DOMContentLoaded', function(event) {
	let convertRuntimeButton = document.getElementById('convertRuntimeButton');
	// fire off the function that outputs runtimes in minute + seconds or just seconds
	convertRuntimeButton.addEventListener('click', function() {
		outputRuntimes();
	});

	// If cursor is in the input field and the enter button is pressed (and released), this simulates a click on the button
	let runtimeInput = document.getElementById('runtime');
	runtimeInput.addEventListener('keyup', (e) => {
		if (e.keyCode === 13) {
			// this is the keyCode for the Enter button
			// Cancel the default action, if needed
			e.preventDefault();
			// Trigger the button element with a click
			convertRuntimeButton.click();
		}
	});

	let calculatePercentButton = document.getElementById('calculatePercentButton');
	// fire off the percentage calculation function
	calculatePercentButton.addEventListener('click', function() {
		outputPercentages();
	});

	// Enables use of enter button for the percentages function as well
	let totalTimeInput = document.getElementById('totalTime');
	totalTimeInput.addEventListener('keyup', (e) => {
		if (e.keyCode === 13) {
			// this is the keyCode for the Enter button
			// Cancel the default action, if needed
			e.preventDefault();
			// Trigger the button element with a click
			calculatePercentButton.click();
		}
	});
});

// takes time as string in #m #s format, then converts it to seconds
let convertToRuntimeSeconds = (runtime) => {
	let minuteMatch = () => {
		if (!runtime.match(RegExp(dataJson.minuteRegex))) {
			return [
				0 //hacky way of making sure a missing m or s doesn't break the function
			];
		} else {
			return runtime.match(RegExp(dataJson.minuteRegex))[1];
		}
	};

	let secondMatch = () => {
		if (!runtime.match(RegExp(dataJson.secondRegex))) {
			return [ 0 ];
		} else {
			return runtime.match(RegExp(dataJson.secondRegex))[1];
		}
	};
	console.log(`minute = ${minuteMatch()} and  seconds = ${secondMatch()}`);
	return minuteMatch() * 60 + secondMatch() * 1;
};

// Converts seconds in minutes and seconds, then returns formatted string
let displayResults = (input) => {
	let minutes = Math.floor(input / 60);
	let seconds = Math.round(input % 60);
	return `${minutes}m ${seconds}s`;
};

// Detects if the container value has s or m in it and then pipes sanitized and rounded value to the convertToRuntimeSeconds function
let formatDetection = (container) => {
	let runtime = document.getElementById(container).value;
	let runtimeSeconds = runtime;
	if (runtime.match(RegExp(dataJson.minuteRegex)) || runtime.match(RegExp(dataJson.secondRegex))) {
		// this block runs if a #m #s format is detected, and converts this to runtimeSeconds
		runtimeSeconds = convertToRuntimeSeconds(runtime);
	}
	return Math.round(runtimeSeconds);
};

// Outputs the 100%, 99% and 95% runtimes
let outputRuntimes = () => {
	let runtimeSeconds = formatDetection('runtime');
	document.getElementById('runtimeDisplay').innerHTML = `
		100% Runtime: ${displayResults(runtimeSeconds)} or ${runtimeSeconds}s<br>
		99% Runtime: ${displayResults(runtimeSeconds * 0.99)} or ${Math.round(runtimeSeconds * 0.99)}s<br> 
		95% Runtime: ${displayResults(runtimeSeconds * 0.95)} or ${Math.round(runtimeSeconds * 0.95)}s<br> 
		50% Runtime: ${displayResults(runtimeSeconds * 0.5)} or ${Math.round(runtimeSeconds * 0.5)}s<br> 
		25% Runtime: ${displayResults(runtimeSeconds * 0.25)} or ${Math.round(runtimeSeconds * 0.25)}s<br> 
	`;
};

// Outputs the % played based on input time played and content length
let outputPercentages = () => {
	let played = formatDetection('timePlayed');
	let total = formatDetection('totalTime');
	let playedPercent = played / total;
	// Gives result rounded to nearest tenth
	document.getElementById('percentDisplay').innerHTML = `
		${Math.round(playedPercent * 1000) / 10}% played
	`;
};
