let dataJson = {
	metadataRegex: '[a-zA-Z]+([0-9]+)',
	channelRegex: 's\\.[\\w\\W]+(channel)[\\s\\:\\"\\=]+([\\w ]+)[\\w\\W]+\\.s',
	pageNameRegex: 's\\.[\\w\\W]+(pageName)[\\s\\:\\"\\=]+([\\w ]+)[\\w\\W]+\\.s',
	// heartbeatRegex: '(\\w+)[\\%:](?:3A)?(\\w+)[\\%:](?:3A)?([\\w\\.]+)(?:[ \\n=]?)([\\w\\%]+)',
	heartbeatRegex: '(s\\:\\w+):?([\\w.]+)\\t(.+)',
	// G1 = full, G2 =  s:meta, G3 = other part of key, G4 = value
	minuteRegex: '(\\d+)?\\s*m',
	secondRegex: '(\\d+)?\\s*s'
};

let allShows = [
	{
		title: '21st Century Elephant',
		showId: '136135',
		slug: '21st-century-elephant'
	},
	{
		title: '40 Under 40',
		showId: '3370283',
		slug: '40-under-40'
	},
	{
		title: '747: The Jumbo Revolution',
		showId: '3407070',
		slug: '747-the-jumbo-revolution'
	},
	{
		title: '9/11: Day That Changed the World',
		showId: '139902',
		slug: '911-day-that-changed-the-world'
	},
	{
		title: '9/11: The Heartland Tapes',
		showId: '3391081',
		slug: '911-the-heartland-tapes'
	},
	{
		title: '9/11: Stories in Fragments',
		showId: '139903',
		slug: '911-stories-in-fragments'
	},
	{
		title: 'A-Bombs Over Nevada',
		showId: '3423161',
		slug: 'abombs-over-nevada'
	},
	{
		title: 'The Accordion Kings',
		showId: '134604',
		slug: 'the-accordion-kings'
	},
	{
		title: 'Adolf Island',
		showId: '3462210',
		slug: 'adolf-island'
	},
	{
		title: 'Aerial Ireland',
		showId: '3434489',
		slug: 'aerial-ireland'
	},
	{
		title: 'Aerial New Zealand',
		showId: '3448728',
		slug: 'aerial-new-zealand'
	},
	{
		title: "Africa's Piranha",
		showId: '3418376',
		slug: 'africas-piranha'
	},
	{
		title: 'Alaskan Summer',
		showId: '3460674',
		slug: 'alaskan-summer'
	},
	{
		title: 'Aliens Inside Us',
		showId: '3379126',
		slug: 'aliens-inside-us'
	},
	{
		title: 'All You Need is Klaus',
		showId: '136655',
		slug: 'all-you-need-is-klaus'
	},
	{
		title: 'Amazing Pigs',
		showId: '3468387',
		slug: 'amazing-pigs'
	},
	{
		title: 'Amazing Plants',
		showId: '136361',
		slug: 'amazing-plants'
	},
	{
		title: 'Amazon River Crocs',
		showId: '137852',
		slug: 'amazon-river-crocs'
	},
	{
		title: 'Americans Underground: Secret City of WWI',
		showId: '3437460',
		slug: 'americans-underground-secret-city-of-wwi'
	},
	{
		title: "America's Badlands",
		showId: '3454768',
		slug: 'americas-badlands'
	},
	{
		title: "America's Greatest Monuments",
		showId: '130198',
		slug: 'americas-greatest-monuments'
	},
	{
		title: "America's Hangar",
		showId: '130144',
		slug: 'americas-hangar'
	},
	{
		title: "America's Lost Submarine",
		showId: '136074',
		slug: 'americas-lost-submarine'
	},
	{
		title: "America's National Parks at 100",
		showId: '3436500',
		slug: 'americas-national-parks-at-100'
	},
	{
		title: "America's Secret D-Day Disaster",
		showId: '3407071',
		slug: 'americas-secret-dday-disaster'
	},
	{
		title: "America's Treasures",
		showId: '130562',
		slug: 'americas-treasures'
	},
	{
		title: "America's Yellowstone",
		showId: '130630',
		slug: 'americas-yellowstone'
	},
	{
		title: 'Amy Winehouse: One Shining Night',
		showId: '3398468',
		slug: 'amy-winehouse-one-shining-night'
	},
	{
		title: 'Animals Aloft',
		showId: '133842',
		slug: 'animals-aloft'
	},
	{
		title: 'Animal Winter Games',
		showId: '134475',
		slug: 'animal-winter-games'
	},
	{
		title: 'Apocalypse: The Second World War: The Inside Story',
		showId: '3359564',
		slug: 'apocalypse-the-second-world-war-the-inside-story'
	},
	{
		title: 'Arabia Uncovered',
		showId: '3379346',
		slug: 'arabia-uncovered'
	},
	{
		title: 'Arlington: Call to Honor',
		showId: '133297',
		slug: 'arlington-call-to-honor'
	},
	{
		title: 'Asian Tsunami: The Deadliest Wave',
		showId: '3415021',
		slug: 'asian-tsunami-the-deadliest-wave'
	},
	{
		title: "Asia's Deadliest Snakes",
		showId: '140632',
		slug: 'asias-deadliest-snakes'
	},
	{
		title: 'Asteroid Trackers',
		showId: '135208',
		slug: 'asteroid-trackers'
	},
	{
		title: 'B2: Stealth at War',
		showId: '3391716',
		slug: 'b2-stealth-at-war'
	},
	{
		title: 'Baboon King',
		showId: '3437510',
		slug: 'baboon-king'
	},
	{
		title: 'Baby New at the Zoo',
		showId: '130204',
		slug: 'baby-new-at-the-zoo'
	},
	{
		title: 'Battle at Sea: Jutland',
		showId: '3439632',
		slug: 'battle-at-sea-jutland'
	},
	{
		title: 'Battle of Little Bighorn',
		showId: '3473007',
		slug: 'battle-of-little-bighorn'
	},
	{
		title: 'Battle of Midway: The True Story',
		showId: '3475927',
		slug: 'battle-of-midway-the-true-story'
	},
	{
		title: 'The Battle of Normandy: 85 Days in Hell',
		showId: '3472892',
		slug: 'the-battle-of-normandy-85-days-in-hell'
	},
	{
		title: 'Battle of Okinawa in Color',
		showId: '3448729',
		slug: 'battle-of-okinawa-in-color'
	},
	{
		title: 'Batwomen of Panama',
		showId: '130310',
		slug: 'batwomen-of-panama'
	},
	{
		title: 'The Big Blue (2007)',
		showId: '131262',
		slug: 'the-big-blue-2007'
	},
	{
		title: 'Big Cats of the Savannah',
		showId: '137541',
		slug: 'big-cats-of-the-savannah'
	},
	{
		title: 'Billionaire Space Club',
		showId: '3457435',
		slug: 'billionaire-space-club'
	},
	{
		title: 'Bird vs. Plane: Miracle on the Hudson',
		showId: '3467432',
		slug: 'bird-vs-plane-miracle-on-the-hudson'
	},
	{
		title: 'Black Hole Hunters',
		showId: '3470276',
		slug: 'black-hole-hunters'
	},
	{
		title: 'Black in Space: Breaking the Color Barrier',
		showId: '3479308',
		slug: 'black-in-space-breaking-the-color-barrier'
	},
	{
		title: 'Black Mamba: Kiss of Death',
		showId: '3396159',
		slug: 'black-mamba-kiss-of-death'
	},
	{
		title: 'Black Wings',
		showId: '141063',
		slug: 'black-wings'
	},
	{
		title: "Blondie's New York",
		showId: '3397952',
		slug: 'blondies-new-york'
	},
	{
		title: 'Blood River Crossing',
		showId: '3379770',
		slug: 'blood-river-crossing'
	},
	{
		title: 'Bombs, Bullets and Fraud',
		showId: '130143',
		slug: 'bombs-bullets-and-fraud'
	},
	{
		title: 'Breath of Freedom',
		showId: '3417370',
		slug: 'breath-of-freedom-1-hour-version'
	},
	{
		title: 'Breath of Freedom (non-1hr)',
		showId: '3402153',
		slug: 'breath-of-freedom'
	},
	{
		title: 'Building Star Trek',
		showId: '3436402',
		slug: 'building-star-trek'
	},
	{
		title: 'Building The Ultimate Telescope',
		showId: '3424959',
		slug: 'building-the-ultimate-telescope'
	},
	{
		title: 'Carrier at War: The USS Enterprise',
		showId: '130371',
		slug: 'carrier-at-war-the-uss-enterprise'
	},
	{
		title: "Castro's Secret Reef",
		showId: '3437500',
		slug: 'castros-secret-reef'
	},
	{
		title: 'CBS News: 50 Years Later, Civil Rights',
		showId: '3411041',
		slug: 'cbs-news-50-years-later-civil-rights'
	},
	{
		title: 'cELLAbration LIVE! A Tribute to Ella Jenkins',
		showId: '133298',
		slug: 'cellabration-live-a-tribute-to-ella-jenkins'
	},
	{
		title: 'Cheetah: Price of Speed',
		showId: '137796',
		slug: 'cheetah-price-of-speed'
	},
	{
		title: 'Cheetah: Race to Rule',
		showId: '3399779',
		slug: 'cheetah-race-to-rule'
	},
	{
		title: 'Chilean Mine Rescue',
		showId: '140284',
		slug: 'chilean-mine-rescue'
	},
	{
		title: "China's Emperor of Evil",
		showId: '3434660',
		slug: 'chinas-emperor-of-evil'
	},
	{
		title: 'Cobra King',
		showId: '3440090',
		slug: 'cobra-king'
	},
	{
		title: 'The Codebreaker Who Hacked Hitler',
		showId: '3422656',
		slug: 'the-codebreaker-who-hacked-hitler'
	},
	{
		title: 'Concorde: Flying Supersonic',
		showId: '136656',
		slug: 'concorde-flying-supersonic'
	},
	{
		title: 'The Coronation',
		showId: '3455604',
		slug: 'the-coronation'
	},
	{
		title: 'Crash Test Heroes: The Dummy Revolution',
		showId: '3393520',
		slug: 'crash-test-heroes-the-dummy-revolution'
	},
	{
		title: 'Crazy Monster Frogs',
		showId: '3412156',
		slug: 'crazy-monster-frogs'
	},
	{
		title: 'Creatures of the Lagoon',
		showId: '141113',
		slug: 'creatures-of-the-lagoon'
	},
	{
		title: 'Crowning New York',
		showId: '3391082',
		slug: 'crowning-new-york'
	},
	{
		title: 'Cruise Ship Down: Saving Concordia',
		showId: '3400919',
		slug: 'cruise-ship-down-saving-concordia'
	},
	{
		title: 'Cutting Loose',
		showId: '130991',
		slug: 'cutting-loose'
	},
	{
		title: 'Dancers on Fire',
		showId: '137551',
		slug: 'dancers-on-fire'
	},
	{
		title: "Darwin & The Beagle's Scandal",
		showId: '3476657',
		slug: 'darwin-the-beagles-scandal'
	},
	{
		title: "The D'Autremont Train Robbery",
		showId: '130225',
		slug: 'the-dautremont-train-robbery'
	},
	{
		title: 'The Da Vinci Detective',
		showId: '137976',
		slug: 'the-da-vinci-detective'
	},
	{
		title: 'The Day The Bomb Dropped',
		showId: '3411676',
		slug: 'the-day-the-bomb-dropped'
	},
	{
		title: 'The Day Hitler Died',
		showId: '3428237',
		slug: 'the-day-hitler-died'
	},
	{
		title: 'The Day Kennedy Died',
		showId: '3387792',
		slug: 'the-day-kennedy-died'
	},
	{
		title: 'Day of the Kamikaze',
		showId: '130631',
		slug: 'day-of-the-kamikaze'
	},
	{
		title: 'The Day We Walked on the Moon',
		showId: '3472207',
		slug: 'the-day-we-walked-on-the-moon'
	},
	{
		title: 'Death Beach',
		showId: '3357376',
		slug: 'death-beach'
	},
	{
		title: 'Decoding Immortality',
		showId: '137613',
		slug: 'decoding-immortality'
	},
	{
		title: 'Desert Warriors: Lions of the Namib',
		showId: '3422790',
		slug: 'desert-warriors-lions-of-the-namib'
	},
	{
		title: 'Designing Dogs',
		showId: '133292',
		slug: 'designing-dogs'
	},
	{
		title: 'Diana and the Paparazzi',
		showId: '3450988',
		slug: 'diana-and-the-paparazzi'
	},
	{
		title: 'Diana: The Day We Said Goodbye',
		showId: '3450989',
		slug: 'diana-the-day-we-said-goodbye'
	},
	{
		title: 'Dino Stampede',
		showId: '138526',
		slug: 'dino-stampede'
	},
	{
		title: 'Diving With Crocodiles',
		showId: '137795',
		slug: 'diving-with-crocodiles'
	},
	{
		title: 'Doomsday in Tunguska',
		showId: '137301',
		slug: 'doomsday-in-tunguska'
	},
	{
		title: "The Doors: Mr. Mojo Risin' -  The Story of L.A. Woman",
		showId: '3361532',
		slug: 'the-doors-mr-mojo-risin-the-story-of-la-woman'
	},
	{
		title: 'Dragon Island',
		showId: '3435534',
		slug: 'dragon-island'
	},
	{
		title: 'Dream Window: Reflections on the Japanese Garden',
		showId: '131418',
		slug: 'dream-window-reflections-on-the-japanese-garden'
	},
	{
		title: 'Earth From Outer Space',
		showId: '3457436',
		slug: 'earth-from-outer-space'
	},
	{
		title: 'Electric Amazon',
		showId: '3425708',
		slug: 'electric-amazon'
	},
	{
		title: 'Electrified: The Guitar Revolution',
		showId: '131283',
		slug: 'electrified-the-guitar-revolution'
	},
	{
		title: 'Elephant King',
		showId: '3373743',
		slug: 'elephant-king'
	},
	{
		title: 'Elephants in the Room',
		showId: '3400293',
		slug: 'elephants-in-the-room'
	},
	{
		title: 'Enemies Within: Joe McCarthy',
		showId: '3364441',
		slug: 'enemies-within-joe-mccarthy'
	},
	{
		title: 'Epic Warrior Women: Vikings',
		showId: '3480402',
		slug: 'viking-women-the-real-valkyries'
	},
	{
		title: 'Escape to the Great Dismal Swamp',
		showId: '3456593',
		slug: 'escape-to-the-great-dismal-swamp'
	},
	{
		title: 'Eyewitness Kamikaze',
		showId: '130819',
		slug: 'eyewitness-kamikaze'
	},
	{
		title: 'Fall of Japan: In Color',
		showId: '3406182',
		slug: 'fall-of-japan-in-color'
	},
	{
		title: 'Finding Life in Outer Space',
		showId: '3458182',
		slug: 'finding-life-in-outer-space'
	},
	{
		title: 'Flying High with Phil Keoghan',
		showId: '3448910',
		slug: 'flying-high-with-phil-keoghan'
	},
	{
		title: 'Flying Monsters with David Attenborough',
		showId: '3432664',
		slug: 'flying-monsters-with-david-attenborough'
	},
	{
		title: 'Flying with Condors',
		showId: '130576',
		slug: 'flying-with-condors'
	},
	{
		title: 'Footprints on the Water: The Nan Hauser Story',
		showId: '132257',
		slug: 'footprints-on-the-water-the-nan-hauser-story'
	},
	{
		title: "Freud's Naked Truths",
		showId: '142150',
		slug: 'freuds-naked-truths'
	},
	{
		title: 'Gallipoli (2009)',
		showId: '135944',
		slug: 'gallipoli-2009'
	},
	{
		title: 'Genius in America',
		showId: '3395004',
		slug: 'genius-in-america'
	},
	{
		title: 'Ghost Cat: Saving the Clouded Leopard',
		showId: '130196',
		slug: 'ghost-cat-saving-the-clouded-leopard'
	},
	{
		title: "The Ghosts of Duffy's Cut",
		showId: '131088',
		slug: 'the-ghosts-of-duffys-cut'
	},
	{
		title: 'The Girl Who Talked to Dolphins',
		showId: '3415122',
		slug: 'the-girl-who-talked-to-dolphins'
	},
	{
		title: 'The Golden Age of Zeppelins',
		showId: '130141',
		slug: 'the-golden-age-of-zeppelins'
	},
	{
		title: 'Goodwood: Race Car Legends',
		showId: '131285',
		slug: 'goodwood-race-car-legends'
	},
	{
		title: 'Gorillas of Gabon',
		showId: '3474724',
		slug: 'gorillas-of-gabon'
	},
	{
		title: 'Goshawk: Soul of the Wind',
		showId: '137606',
		slug: 'goshawk-soul-of-the-wind'
	},
	{
		title: "The Gospel of Jesus's Wife",
		showId: '3365413',
		slug: 'the-gospel-of-jesuss-wife'
	},
	{
		title: 'The Grammar of Happiness',
		showId: '141519',
		slug: 'the-grammar-of-happiness'
	},
	{
		title: 'Great Snakes',
		showId: '3419987',
		slug: 'great-snakes'
	},
	{
		title: 'Great White Code Red',
		showId: '3402306',
		slug: 'great-white-code-red'
	},
	{
		title: 'The Green Book: Guide to Freedom',
		showId: '3467847',
		slug: 'the-green-book-guide-to-freedom'
	},
	{
		title: 'Grizzly',
		showId: '136103',
		slug: 'grizzly'
	},
	{
		title: 'Gun Trucks of Vietnam',
		showId: '3462307',
		slug: 'gun-trucks-of-vietnam'
	},
	{
		title: "Haiti's Treasures: Out of the Rubble",
		showId: '3358161',
		slug: 'haitis-treasures-out-of-the-rubble'
	},
	{
		title: "Hannibal's March on Rome",
		showId: '3478944',
		slug: 'hannibals-march-on-rome'
	},
	{
		title: 'Haunt of the Harpy',
		showId: '137810',
		slug: 'haunt-of-the-harpy'
	},
	{
		title: 'Hindenburg: The Untold Story',
		showId: '130593',
		slug: 'hindenburg-the-untold-story'
	},
	{
		title: 'Hip Hop: The Furious Force of Rhymes',
		showId: '138801',
		slug: 'hip-hop-the-furious-force-of-rhymes'
	},
	{
		title: 'Hippo Ganglands',
		showId: '3404938',
		slug: 'hippo-ganglands'
	},
	{
		title: 'Hippos After Dark',
		showId: '3439344',
		slug: 'hippos-after-dark'
	},
	{
		title: "Hitler's Riches",
		showId: '3404939',
		slug: 'hitlers-riches'
	},
	{
		title: 'The Hittites',
		showId: '135943',
		slug: 'the-hittites'
	},
	{
		title: 'Hockney',
		showId: '3437511',
		slug: 'hockney'
	},
	{
		title: 'Hockney: Seeing Beauty',
		showId: '3440616',
		slug: 'hockney-seeing-beauty'
	},
	{
		title: 'Honey Badger Grit',
		showId: '3450985',
		slug: 'honey-badger-grit'
	},
	{
		title: 'Hover Racers: Flying on Air',
		showId: '131739',
		slug: 'hover-racers-flying-on-air'
	},
	{
		title: 'How to Clone a Woolly Mammoth',
		showId: '3415041',
		slug: 'how-to-clone-a-woolly-mammoth'
	},
	{
		title: 'Hozier: The Church Tapes',
		showId: '3427105',
		slug: 'hozier-the-church-tapes'
	},
	{
		title: 'Humboldt: Epic Explorer',
		showId: '3476394',
		slug: 'humboldt-epic-explorer'
	},
	{
		title: 'The Hunt for Bin Laden',
		showId: '141475',
		slug: 'the-hunt-for-bin-laden'
	},
	{
		title: 'Hunt for the Double Eagle',
		showId: '133183',
		slug: 'hunt-for-the-double-eagle'
	},
	{
		title: 'Hunt for the Nazi Gold Train',
		showId: '3450984',
		slug: 'hunt-for-the-nazi-gold-train'
	},
	{
		title: 'Hunt for the Super Predator',
		showId: '3400292',
		slug: 'hunt-for-the-super-predator'
	},
	{
		title: 'Hunting the Hammerhead',
		showId: '3424960',
		slug: 'hunting-the-hammerhead'
	},
	{
		title: 'Ice Bridge: The Impossible Journey',
		showId: '3458237',
		slug: 'ice-bridge-the-impossible-journey'
	},
	{
		title: 'iCrocodile',
		showId: '3428432',
		slug: 'icrocodile'
	},
	{
		title: 'Incredible Animal Moments',
		showId: '3468386',
		slug: 'incredible-animal-moments'
	},
	{
		title: 'The Incredible Bionic Man',
		showId: '3378516',
		slug: 'the-incredible-bionic-man'
	},
	{
		title: 'Incredible Flying Jet Packs',
		showId: '3416426',
		slug: 'incredible-flying-jet-packs'
	},
	{
		title: 'In Search of Santa Claus',
		showId: '131259',
		slug: 'in-search-of-santa-claus'
	},
	{
		title: 'Insect Microcosm',
		showId: '131084',
		slug: 'insect-microcosm'
	},
	{
		title: 'Into The Frozen Abyss',
		showId: '135041',
		slug: 'into-the-frozen-abyss'
	},
	{
		title: 'Islands of Creation',
		showId: '3421513',
		slug: 'islands-of-creation'
	},
	{
		title: 'Islands of Fire',
		showId: '3474723',
		slug: 'islands-of-fire'
	},
	{
		title: 'Isle of Chimps',
		showId: '3474725',
		slug: 'isle-of-chimps'
	},
	{
		title: 'I Was a Jet Set Stewardess',
		showId: '3407792',
		slug: 'i-was-a-jet-set-stewardess'
	},
	{
		title: 'Jago: A Life Underwater',
		showId: '3437527',
		slug: 'jago-a-life-underwater'
	},
	{
		title: 'Japanese Bow: Built to Kill',
		showId: '135057',
		slug: 'japanese-bow-built-to-kill'
	},
	{
		title: "Jefferson's Secret Bible",
		showId: '140747',
		slug: 'jeffersons-secret-bible'
	},
	{
		title: "Kennedy's Suicide Bomber",
		showId: '3395002',
		slug: 'kennedys-suicide-bomber'
	},
	{
		title: 'Killer Hornets',
		showId: '3436503',
		slug: 'killer-hornets'
	},
	{
		title: 'Killer in the Caves',
		showId: '3374278',
		slug: 'killer-in-the-caves'
	},
	{
		title: 'Kingfisher',
		showId: '137548',
		slug: 'kingfisher'
	},
	{
		title: 'King of the Desert Lions',
		showId: '3461473',
		slug: 'king-of-the-desert-lions'
	},
	{
		title: 'Kings of the Desert',
		showId: '3461474',
		slug: 'kings-of-the-desert'
	},
	{
		title: 'Kings of the Prairie',
		showId: '137539',
		slug: 'kings-of-the-prairie'
	},
	{
		title: "The King's Skeleton: Richard III Revealed",
		showId: '3381029',
		slug: 'the-kings-skeleton-richard-iii-revealed'
	},
	{
		title: "The King's Speech Revealed",
		showId: '139844',
		slug: 'the-kings-speech-revealed'
	},
	{
		title: "King Tut's Final Mystery",
		showId: '3411477',
		slug: 'king-tuts-final-mystery'
	},
	{
		title: 'Kuru: The Science and the Sorcery',
		showId: '137793',
		slug: 'kuru-the-science-and-the-sorcery'
	},
	{
		title: 'The Last Buffalo',
		showId: '3434665',
		slug: 'the-last-buffalo'
	},
	{
		title: 'Last Emperor of Mexico',
		showId: '3440617',
		slug: 'last-emperor-of-mexico'
	},
	{
		title: 'Laws of the Lizard',
		showId: '3459142',
		slug: 'law-of-the-lizards'
	},
	{
		title: 'Leaving Earth: Or How to Colonize a Planet',
		showId: '3458214',
		slug: 'leaving-earth-or-how-to-colonize-a-planet'
	},
	{
		title: 'The Leeper Mail Bomb',
		showId: '130226',
		slug: 'the-leeper-mail-bomb'
	},
	{
		title: 'Legendary Coins of the Smithsonian',
		showId: '130309',
		slug: 'legendary-coins-of-the-smithsonian'
	},
	{
		title: 'Legend of the Crystal Skulls',
		showId: '133348',
		slug: 'legend-of-the-crystal-skulls'
	},
	{
		title: 'Legend of Lead Belly',
		showId: '3416955',
		slug: 'legend-of-lead-belly'
	},
	{
		title: 'Leopard Fight Club',
		showId: '3404936',
		slug: 'leopard-fight-club'
	},
	{
		title: 'Leopard Huntress',
		showId: '3437509',
		slug: 'leopard-huntress'
	},
	{
		title: 'The Leopard Rocks',
		showId: '3477815',
		slug: 'the-leopard-rocks'
	},
	{
		title: 'Lethal Attractions',
		showId: '3412157',
		slug: 'lethal-attractions'
	},
	{
		title: 'A Life Among Monkeys ',
		showId: '3470249',
		slug: 'a-life-among-monkeys'
	},
	{
		title: "Lincoln's Last Day",
		showId: '3419641',
		slug: 'lincolns-last-day'
	},
	{
		title: "Lincoln's Washington at War",
		showId: '3374628',
		slug: 'lincolns-washington-at-war'
	},
	{
		title: 'Lions in Battle',
		showId: '137794',
		slug: 'lions-in-battle'
	},
	{
		title: 'Lions in Battle 2',
		showId: '3402386',
		slug: 'lions-in-battle-2'
	},
	{
		title: 'Lions Unleashed',
		showId: '3445725',
		slug: 'lions-unleashed'
	},
	{
		title: 'Lives That Changed the World: Nelson Mandela',
		showId: '3401408',
		slug: 'lives-that-changed-the-world-nelson-mandela'
	},
	{
		title: 'The Lockerbie Bombing',
		showId: '3398469',
		slug: 'the-lockerbie-bombing'
	},
	{
		title: 'Loose at the Zoo',
		showId: '130193',
		slug: 'loose-at-the-zoo'
	},
	{
		title: 'Lost City of Gladiators',
		showId: '3436502',
		slug: 'lost-city-of-gladiators'
	},
	{
		title: "The Maharajas' Motor Car",
		showId: '137811',
		slug: 'the-maharajas-motor-car'
	},
	{
		title: 'Maira Kalman: My Favorite Things',
		showId: '3418591',
		slug: 'maira-kalman-my-favorite-things'
	},
	{
		title: 'Making The Monkees',
		showId: '133957',
		slug: 'making-the-monkees'
	},
	{
		title: 'Malaysia 370: The Plane That Vanished',
		showId: '3406766',
		slug: 'malaysia-370-the-plane-that-vanished'
	},
	{
		title: 'Mammoths: Giants of the Ice Age',
		showId: '3419961',
		slug: 'mammoths-giants-of-the-ice-age'
	},
	{
		title: 'Marilyn Monroe for Sale',
		showId: '3466376',
		slug: 'marilyn-monroe-for-sale'
	},
	{
		title: 'Mass Extinction: Life at the Brink',
		showId: '3413789',
		slug: 'mass-extinction-life-at-the-brink'
	},
	{
		title: 'Meet the Meerkats',
		showId: '3365767',
		slug: 'meet-the-meerkats'
	},
	{
		title: 'Memphis Belle in Color',
		showId: '3475928',
		slug: 'memphis-belle-in-color'
	},
	{
		title: 'The Men Who Brought the Dawn',
		showId: '131130',
		slug: 'the-men-who-brought-the-dawn'
	},
	{
		title: 'Merlin: The Legend',
		showId: '136112',
		slug: 'merlin-the-legend'
	},
	{
		title: 'Mighty Planes: Heroic Hercules',
		showId: '3440479',
		slug: 'mighty-planes-heroic-hercules'
	},
	{
		title: 'Mighty Ship at War: Queen Mary',
		showId: '3436505',
		slug: 'mighty-ship-at-war-queen-mary'
	},
	{
		title: 'Million Dollar American Princesses: Meghan Markle',
		showId: '3459289',
		slug: 'million-dollar-american-princesses-meghan-markle'
	},
	{
		title: 'Mission Critical: Amphibian Rescue',
		showId: '137757',
		slug: 'mission-critical-amphibian-rescue'
	},
	{
		title: 'Mission to Murder Hitler',
		showId: '137276',
		slug: 'mission-to-murder-hitler'
	},
	{
		title: 'MLK: The Assassination Tapes',
		showId: '141155',
		slug: 'mlk-the-assassination-tapes'
	},
	{
		title: 'Moby Dick: Heart of a Whale',
		showId: '3423203',
		slug: 'moby-dick-heart-of-a-whale'
	},
	{
		title: 'Monster Shipwreck: Mystery of the Mars',
		showId: '3462312',
		slug: 'monster-shipwreck-mystery-of-the-mars'
	},
	{
		title: 'The Mountain Lion and Me',
		showId: '3457437',
		slug: 'the-mountain-lion-and-me'
	},
	{
		title: 'My Big Bollywood Wedding',
		showId: '3437452',
		slug: 'my-big-bollywood-wedding'
	},
	{
		title: 'My Journey with a Polar Bear',
		showId: '3468389',
		slug: 'my-journey-with-a-polar-bear'
	},
	{
		title: 'Mysteries of the Rainforest',
		showId: '3445645',
		slug: 'mysteries-of-the-rainforest'
	},
	{
		title: 'Mystery In Yellowstone',
		showId: '3426461',
		slug: 'mystery-in-yellowstone'
	},
	{
		title: 'Mystery of the Hope Diamond',
		showId: '136360',
		slug: 'mystery-of-the-hope-diamond'
	},
	{
		title: 'Mystery of the Lost Pyramid',
		showId: '3478945',
		slug: 'mystery-of-the-lost-pyramid'
	},
	{
		title: 'Mystery of the Nazca Lines',
		showId: '137271',
		slug: 'mystery-of-the-nazca-lines'
	},
	{
		title: 'Mystical Journey: Kumbh Mela',
		showId: '3392920',
		slug: 'mystical-journey-kumbh-mela'
	},
	{
		title: 'Naked Mole Rats',
		showId: '3468388',
		slug: 'naked-mole-rats'
	},
	{
		title: "Namibia's Wild Wonders",
		showId: '140847',
		slug: 'namibias-wild-wonders'
	},
	{
		title: "Napoleon's Waterloo",
		showId: '3421512',
		slug: 'napoleons-waterloo'
	},
	{
		title: "Nature's Dive Bombers",
		showId: '137714',
		slug: 'natures-dive-bombers'
	},
	{
		title: "Nature's Matchmaker",
		showId: '137812',
		slug: 'natures-matchmaker'
	},
	{
		title: 'Nazi Temple of Doom',
		showId: '3379771',
		slug: 'nazi-temple-of-doom'
	},
	{
		title: 'Nepal Quake: Terror On Everest',
		showId: '3423023',
		slug: 'nepal-quake-terror-on-everest'
	},
	{
		title: 'Nightmare on Everest',
		showId: '3446793',
		slug: 'nightmare-on-everest'
	},
	{
		title: 'Ninja: Shadow Warriors',
		showId: '141184',
		slug: 'ninja-shadow-warriors'
	},
	{
		title: 'The North Pole Conspiracy',
		showId: '137272',
		slug: 'the-north-pole-conspiracy'
	},
	{
		title: 'Oasis Earth',
		showId: '134469',
		slug: 'oasis-earth'
	},
	{
		title: 'The Obama Years: The Power of Words',
		showId: '3444572',
		slug: 'the-obama-years-the-power-of-words'
	},
	{
		title: 'An Ocean Mystery: The Missing Catch',
		showId: '3437476',
		slug: 'an-ocean-mystery-the-missing-catch'
	},
	{
		title: 'The Origins of Oz',
		showId: '137669',
		slug: 'the-origins-of-oz'
	},
	{
		title: "Panama's Animal Highway",
		showId: '3437451',
		slug: 'panamas-animal-highway'
	},
	{
		title: 'Panda Breeding Diary',
		showId: '136102',
		slug: 'panda-breeding-diary'
	},
	{
		title: 'Pandas in the Wild',
		showId: '130140',
		slug: 'pandas-in-the-wild'
	},
	{
		title: 'Paris Terror Attack: Charlie Hebdo',
		showId: '3429186',
		slug: 'paris-terror-attack-charlie-hebdo'
	},
	{
		title: 'The Perfect Runner',
		showId: '3393860',
		slug: 'the-perfect-runner'
	},
	{
		title: 'Phil Collins: Going Back to Detroit',
		showId: '137611',
		slug: 'phil-collins-going-back-to-detroit'
	},
	{
		title: 'Picturing the President: George Washington',
		showId: '131379',
		slug: 'picturing-the-president-george-washington'
	},
	{
		title: 'Picturing the Presidents',
		showId: '132165',
		slug: 'picturing-the-presidents'
	},
	{
		title: 'Planes, Cranes and Rockets',
		showId: '3402081',
		slug: 'planes-cranes-and-rockets'
	},
	{
		title: 'Play On, John: A Life in Music',
		showId: '131378',
		slug: 'play-on-john-a-life-in-music'
	},
	{
		title: 'Pocahontas: Beyond the Myth',
		showId: '3445499',
		slug: 'pocahontas-beyond-the-myth'
	},
	{
		title: 'Polar Bear: Living on Thin Ice',
		showId: '136129',
		slug: 'polar-bear-living-on-thin-ice'
	},
	{
		title: 'Polar Bear Summer',
		showId: '3422027',
		slug: 'polar-bear-summer'
	},
	{
		title: 'Pompeii: The Dead Speak',
		showId: '3436504',
		slug: 'pompeii-the-dead-speak'
	},
	{
		title: 'Portrait of Artistic Genius: Katsushika Hokusai',
		showId: '130497',
		slug: 'portrait-of-artistic-genius-katsushika-hokusai'
	},
	{
		title: 'The Predator Coast',
		showId: '3363241',
		slug: 'the-predator-coast'
	},
	{
		title: "Princess Diana's 'Wicked' Stepmother",
		showId: '3474722',
		slug: 'princess-dianas-wicked-stepmother'
	},
	{
		title: 'The Queen at 90',
		showId: '3433630',
		slug: 'the-queen-at-90'
	},
	{
		title: 'Queen of the Pythons',
		showId: '3439641',
		slug: 'queen-of-the-pythons'
	},
	{
		title: 'Raiders of the Jade Empire',
		showId: '3425696',
		slug: 'raiders-of-the-jade-empire'
	},
	{
		title: 'The Real Beauty and the Beast',
		showId: '3394874',
		slug: 'the-real-beauty-and-the-beast'
	},
	{
		title: 'The Real Lion Queen',
		showId: '3400415',
		slug: 'the-real-lion-queen'
	},
	{
		title: 'The Real World of Peter Gabriel',
		showId: '137797',
		slug: 'the-real-world-of-peter-gabriel'
	},
	{
		title: 'Remembering Vietnam: The Wall at 25',
		showId: '131377',
		slug: 'remembering-vietnam-the-wall-at-25'
	},
	{
		title: 'The Rise of the Killer Virus',
		showId: '3415042',
		slug: 'the-rise-of-the-killer-virus'
	},
	{
		title: 'Rise of Tokyo in Color',
		showId: '3460342',
		slug: 'rise-of-tokyo-in-color'
	},
	{
		title: 'The Rivals',
		showId: '135757',
		slug: 'the-rivals'
	},
	{
		title: 'Roadsworth: Crossing the Line',
		showId: '135966',
		slug: 'roadsworth-crossing-the-line'
	},
	{
		title: 'Rocking the Opera House: Dr. John',
		showId: '3374283',
		slug: 'rocking-the-opera-house-dr-john'
	},
	{
		title: 'Running with Wolves',
		showId: '135974',
		slug: 'running-with-wolves'
	},
	{
		title: 'Sacred Sites: Ireland',
		showId: '3408479',
		slug: 'sacred-sites-ireland'
	},
	{
		title: 'Samurai Headhunters',
		showId: '3391088',
		slug: 'samurai-headhunters'
	},
	{
		title: 'Samurai Warrior Queens',
		showId: '3420808',
		slug: 'samurai-warrior-queens'
	},
	{
		title: 'Saving Treasures',
		showId: '130282',
		slug: 'saving-treasures'
	},
	{
		title: 'SEAL Dog',
		showId: '3418375',
		slug: 'seal-dog'
	},
	{
		title: 'Secret Life of the Rainforest',
		showId: '141211',
		slug: 'secret-life-of-the-rainforest'
	},
	{
		title: 'Secrets of the Great Barrier Reef',
		showId: '134605',
		slug: 'secrets-of-the-great-barrier-reef'
	},
	{
		title: 'Secrets of the Hive',
		showId: '3417615',
		slug: 'secrets-of-the-hive'
	},
	{
		title: 'Secrets of Shark Island',
		showId: '3423862',
		slug: 'secrets-of-shark-island'
	},
	{
		title: 'Secrets of the Taj Mahal',
		showId: '137191',
		slug: 'secrets-of-the-taj-mahal'
	},
	{
		title: 'Seed Hunter',
		showId: '134427',
		slug: 'seed-hunter'
	},
	{
		title: 'Seizing Justice: The Greensboro 4',
		showId: '136657',
		slug: 'seizing-justice-the-greensboro-4'
	},
	{
		title: 'The Seven Dwarfs of Auschwitz',
		showId: '3404239',
		slug: 'the-seven-dwarfs-of-auschwitz'
	},
	{
		title: "Shackleton's Frozen Hell",
		showId: '3370282',
		slug: 'shackletons-frozen-hell'
	},
	{
		title: 'Shark Girl',
		showId: '3406405',
		slug: 'shark-girl'
	},
	{
		title: 'Shark Therapy',
		showId: '133349',
		slug: 'shark-therapy'
	},
	{
		title: 'A Shot to Save the World ',
		showId: '3377766',
		slug: 'a-shot-to-save-the-world'
	},
	{
		title: "Shuttle Discovery's Last Mission",
		showId: '3368493',
		slug: 'shuttle-discoverys-last-mission'
	},
	{
		title: 'Siege of Masada',
		showId: '3417626',
		slug: 'siege-of-masada'
	},
	{
		title: 'Silicon Valley Rebels',
		showId: '3361661',
		slug: 'silicon-valley-rebels'
	},
	{
		title: 'Sinking The Lusitania: An American Tragedy',
		showId: '3420538',
		slug: 'sinking-the-lusitania-an-american-tragedy'
	},
	{
		title: 'Skateboard Nation',
		showId: '141110',
		slug: 'skateboard-nation'
	},
	{
		title: 'Skin Deep',
		showId: '141379',
		slug: 'skin-deep'
	},
	{
		title: "Smithsonian's Weirdest",
		showId: '130561',
		slug: 'smithsonians-weirdest'
	},
	{
		title: 'Smithsonian Time Capsule: 1968',
		showId: '3459167',
		slug: 'smithsonian-time-capsule-1968'
	},
	{
		title: "Soul of a People: Writing America's Story",
		showId: '135396',
		slug: 'soul-of-a-people-writing-americas-story'
	},
	{
		title: 'South Sea Pearls',
		showId: '134682',
		slug: 'south-sea-pearls'
	},
	{
		title: 'Space Shuttle: Final Countdown',
		showId: '140673',
		slug: 'space-shuttle-final-countdown'
	},
	{
		title: 'The Spy in the Hanoi Hilton',
		showId: '3419642',
		slug: 'the-spy-in-the-hanoi-hilton'
	},
	{
		title: 'Stand Up To Cancer 2012',
		showId: '3362729',
		slug: 'stand-up-to-cancer-2012'
	},
	{
		title: 'Stand Up To Cancer 2014',
		showId: '3411601',
		slug: 'stand-up-to-cancer-2014'
	},
	{
		title: 'A Star-Spangled Story: Battle for America',
		showId: '3407072',
		slug: 'a-starspangled-story-battle-for-america'
	},
	{
		title: 'Stealing Shakespeare',
		showId: '137612',
		slug: 'stealing-shakespeare'
	},
	{
		title: 'Stealth: Flying Invisible',
		showId: '131317',
		slug: 'stealth-flying-invisible'
	},
	{
		title: 'Stonehenge Deciphered',
		showId: '133692',
		slug: 'stonehenge-deciphered'
	},
	{
		title: 'Stonehenge Empire',
		showId: '3407073',
		slug: 'stonehenge-empire'
	},
	{
		title: 'Stranded: Alpine Air Crash',
		showId: '3396385',
		slug: 'stranded-alpine-air-crash'
	},
	{
		title: 'The Supercar Story',
		showId: '130308',
		slug: 'the-supercar-story'
	},
	{
		title: 'Surviving the Serengeti',
		showId: '3416427',
		slug: 'surviving-the-serengeti'
	},
	{
		title: 'The Sweet Lady with the Nasty Voice',
		showId: '131421',
		slug: 'the-sweet-lady-with-the-nasty-voice'
	},
	{
		title: 'Tanks of Fury',
		showId: '3415077',
		slug: 'tanks-of-fury'
	},
	{
		title: 'Tattoo Odyssey',
		showId: '136658',
		slug: 'tattoo-odyssey'
	},
	{
		title: 'The Teacher Who Defied Hitler',
		showId: '3380909',
		slug: 'the-teacher-who-defied-hitler'
	},
	{
		title: 'Thunderheads',
		showId: '133543',
		slug: 'thunderheads'
	},
	{
		title: 'Tiger on the Run',
		showId: '3423024',
		slug: 'tiger-on-the-run'
	},
	{
		title: 'Tiger Tales',
		showId: '130203',
		slug: 'tiger-tales'
	},
	{
		title: 'Timewatch: D-Day: The True Story of Omaha',
		showId: '132870',
		slug: 'timewatch-dday-the-true-story-of-omaha'
	},
	{
		title: "Timewatch: The Rebel Pharaoh's Lost City",
		showId: '133312',
		slug: 'timewatch-the-rebel-pharaohs-lost-city'
	},
	{
		title: 'Titanic: The New Evidence',
		showId: '3445002',
		slug: 'titanic-the-new-evidence'
	},
	{
		title: "Titanic's Fatal Fire",
		showId: '3439558',
		slug: 'titanics-fatal-fire'
	},
	{
		title: "Titanic's Final Mystery",
		showId: '141474',
		slug: 'titanics-final-mystery'
	},
	{
		title: 'Titanoboa: Monster Snake',
		showId: '140671',
		slug: 'titanoboa-monster-snake'
	},
	{
		title: 'Top Ten Deadliest Beasts',
		showId: '3462226',
		slug: 'top-ten-deadliest-beasts'
	},
	{
		title: "Treblinka: Hitler's Killing Machine",
		showId: '3403868',
		slug: 'treblinka-hitlers-killing-machine'
	},
	{
		title: 'The True Story of the Mary Celeste',
		showId: '130102',
		slug: 'the-true-story-of-the-mary-celeste'
	},
	{
		title: 'Turf War: Lions and Hippos',
		showId: '3388516',
		slug: 'turf-war-lions-and-hippos'
	},
	{
		title: 'The Ultimate Gun Room',
		showId: '130505',
		slug: 'the-ultimate-gun-room'
	},
	{
		title: 'Unbelievable Flying Objects',
		showId: '131060',
		slug: 'unbelievable-flying-objects'
	},
	{
		title: 'Uncommon Courage: Breakout at Chosin',
		showId: '136060',
		slug: 'uncommon-courage-breakout-at-chosin'
	},
	{
		title: 'United States of Drinking',
		showId: '3413913',
		slug: 'united-states-of-drinking'
	},
	{
		title: 'The Unknown Flag Raiser of Iwo Jima',
		showId: '3435576',
		slug: 'the-unknown-flag-raiser-of-iwo-jima'
	},
	{
		title: 'Unleashed: A Dogumentary',
		showId: '134423',
		slug: 'unleashed-a-dogumentary'
	},
	{
		title: 'V2: Nazi Rocket',
		showId: '3423162',
		slug: 'v2-nazi-rocket'
	},
	{
		title: 'The Vampire Princess',
		showId: '131383',
		slug: 'the-vampire-princess'
	},
	{
		title: 'Venom Islands',
		showId: '3357375',
		slug: 'venom-islands'
	},
	{
		title: 'Victorian Rebel: Marianne North',
		showId: '3445576',
		slug: 'victorian-rebel-marianne-north'
	},
	{
		title: 'Viper Queens',
		showId: '3435533',
		slug: 'viper-queens'
	},
	{
		title: 'Volcanoes: Dual Destruction',
		showId: '3473006',
		slug: 'volcanoes-dual-destruction'
	},
	{
		title: 'Waco: The Longest Siege',
		showId: '3458247',
		slug: 'waco-the-longest-siege'
	},
	{
		title: 'Wanted: Anaconda',
		showId: '130594',
		slug: 'wanted-anaconda'
	},
	{
		title: 'Warriors of the Kalahari',
		showId: '137552',
		slug: 'warriors-of-the-kalahari'
	},
	{
		title: 'When Lions Attack',
		showId: '3392671',
		slug: 'when-lions-attack'
	},
	{
		title: 'When Pigs Fly',
		showId: '129993',
		slug: 'when-pigs-fly'
	},
	{
		title: 'When Whales Walked: Journeys in Deep Time',
		showId: '3471672',
		slug: 'when-whales-walked-journeys-in-deep-time'
	},
	{
		title: 'White House Revealed',
		showId: '134611',
		slug: 'white-house-revealed'
	},
	{
		title: 'Wings of Honor',
		showId: '135040',
		slug: 'wings-of-honor'
	},
	{
		title: 'Wolf vs. Bear',
		showId: '3469228',
		slug: 'wolf-vs-bear'
	},
	{
		title: 'A Woman Among Wolves',
		showId: '130894',
		slug: 'a-woman-among-wolves'
	},
	{
		title: "World's Biggest Beasts",
		showId: '3425021',
		slug: 'worlds-biggest-beasts'
	},
	{
		title: "World's Finest Cars: The Insider's Guide",
		showId: '130103',
		slug: 'worlds-finest-cars-the-insiders-guide'
	},
	{
		title: 'Worlds of Sound: The Ballad of Folkways',
		showId: '135487',
		slug: 'worlds-of-sound-the-ballad-of-folkways'
	},
	{
		title: "World's Smallest Planes",
		showId: '130197',
		slug: 'worlds-smallest-planes'
	},
	{
		title: 'World War II Spy School',
		showId: '3416231',
		slug: 'world-war-ii-spy-school'
	},
	{
		title: 'The Wyeths: A Father and His Family',
		showId: '131419',
		slug: 'the-wyeths-a-father-and-his-family'
	},
	{
		title: 'Yellowstone Supervolcano',
		showId: '3402154',
		slug: 'yellowstone-supervolcano'
	},
	{
		title: 'Zambezi',
		showId: '134536',
		slug: 'zambezi'
	}
];
