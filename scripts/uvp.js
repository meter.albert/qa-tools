document.addEventListener('DOMContentLoaded', function(event) {
	addEL(dqs('#uvpButton'), 'click', uvpFormat);
	addEL(dqs('#clearTableButton'), 'click', clearTable);
});

class metadata {
	constructor(legacyName, uvpName, path, notes, sessionStart, play, sessionComplete) {
		this.legacyName = legacyName;
		this.uvpName = uvpName;
		this.path = path;
		this.notes = notes;
		// might want to include sessionStart, play, etc as booleans to help determine where they should show up
	}
}
// based on ENH's Rosetta Stone worksheet
let params = [
	[ 's.prop1', 'customMetadata.s.prop1' ],
	[ 's.prop2', 'customMetadata.s.prop2' ],
	[ 's.prop3', 'customMetadata.s.prop3' ],
	[ 's.prop4', 'customMetadata.s.prop4' ],
	[ 's.prop5', 'customMetadata.s.prop5' ],
	[ 's.prop6', 'customMetadata.s.prop6' ],
	[ 's.prop7', 'customMetadata.s.prop7' ],
	[ 's.prop8', 'customMetadata.s.prop8' ],
	[ 's.prop11', 'customMetadata.s.prop11' ],
	[ 's.prop15', 'customMetadata.s.prop15' ],
	[ 's.prop20', 'customMetadata.s.prop20' ],
	[ 's.prop22', 'customMetadata.s.prop22' ],
	[ 's.prop23', 'customMetadata.s.prop23' ],
	[ 's.prop24', 'customMetadata.s.prop24' ],
	[ 's.prop38', 'customMetadata.s.prop38' ],
	[ 'eVar1', 'customMetadata.eVar1' ],
	[ 'eVar2', 'customMetadata.eVar2' ],
	[ 'eVar3', 'customMetadata.eVar3' ],
	[ 'eVar4', 'customMetadata.eVar4' ],
	[ 'eVar5', 'customMetadata.eVar5' ],
	[ 'eVar6', 'customMetadata.eVar6' ],
	[ 'eVar7', 'customMetadata.eVar7' ],
	[ 'eVar8', 'customMetadata.eVar8' ],
	[ 'eVar11', 'customMetadata.eVar11' ],
	[ 'eVar15', 'customMetadata.eVar15' ],
	[ 'eVar20', 'customMetadata.eVar20' ],
	[ 'eVar22', 'customMetadata.eVar22' ],
	[ 'eVar23', 'customMetadata.eVar23' ],
	[ 'eVar24', 'customMetadata.eVar24' ],
	[ 'eVar38', 'customMetadata.eVar38' ],
	[ 'h:sc:ssl', 'params.analytics.enableSSL' ],
	[ 'l:asset:ad_length', 'params.media.ad.length' ],
	[ 'l:asset:length', 'n/a', '' ],
	[ 'l:asset:pod_offset', 'params.media.ad.podIndex' ],
	[ 'l:event:duration', 'n/a', 'Probably implementation detail' ],
	[ 'l:event:playhead', 'playerTime.playhead' ],
	[ 'l:event:ts', 'playerTime.ts' ],
	[ 'l:sp:hb_api_lvl', 'n/a', 'Probably implementation detail' ],
	[ 'l:stream:bitrate', 'n/a', 'Not used in legacy.' ],
	[ 'l:stream:dropped_frames', 'n/a', 'Not used in legacy.' ],
	[ 'l:stream:fps', 'n/a', 'Not used in legacy.' ],
	[ 'l:stream:startup_time', 'n/a', 'Not used in legacy.' ],
	[ 'n/a', 'l:aam:loc_hint', 'not sure what this is' ],
	[ 'n/a', 'params.media.ad.playerName' ],
	[ 'n/a', 'params.media.ad.podFriendlyName', 'Not in legacy implementation. Pod seems to be named after the ad.'],
	[ 'n/a', 'params.media.ad.podSecond' ],
	[ 's:aam:blob', 'n/a', 'Not sure what this is' ],
	[ 's:aam:blob', 'n/a', 'Not sure what this is' ],
	[ 's:asset:ad_id', 'params.media.ad.id' ],
	[ 's:asset:ad_name', 'params.media.ad.name' ],
	[ 's:asset:ad_sid', 'n/a', 'missing mapping' ],
	[ 's:asset:name', 'n/a', 'Redundant with friendlyName' ],
	[ 's:asset:pod_id', 'n/a' ],
	[ 's:asset:pod_name', 'n/a' ],
	[ 's:asset:pod_position', 'params.media.ad.podPosition', 'Not in legacy implementation.' ],
	[ 's:asset:publisher', 'params.visitor.marketingCloudOrgId' ],
	[ 's:asset:type', 'n/a', 'Not sure what this is' ],
	[ 's:asset:video_id', 'params.media.assetId' ],
	[ 's:event:sid', 'n/a', 'Probably implementation detail' ],
	[ 's:event:type', 'eventType' ],
	[ 's:meta:a.media.airDate', 'params.media.firstAirDate' ],
	[ 's:meta:a.media.asset', 'n/a', 'Redundant with params.media.assetId' ],
	[ 's:meta:a.media.episode', 'customMetadata.a.media.episode' ],
	[ 's:meta:a.media.feed', 'params.media.feed' ],
	[ 's:meta:a.media.format', 'n/a', 'Has value of vod, not sure if relevant' ],
	[ 's:meta:a.media.format', 'n/a', 'Has value of vod, not sure if relevant' ],
	[ 's:meta:a.media.friendlyName', 'params.media.name' ],
	[ 's:meta:a.media.genre', 'params.media.genre' ],
	[ 's:meta:a.media.length', 'params.media.length' ],
	[ 's:meta:a.media.name', 'params.media.id' ],
	[ 's:meta:a.media.playerName', 'params.media.playerName' ],
	[ 's:meta:a.media.rating', 'params.media.rating' ],
	[ 's:meta:a.media.season', 'customMetadata.a.media.season' ],
	[ 's:meta:a.media.show', 'params.media.show' ],
	[ 's:meta:a.media.streamType', 'n/a', 'Not sure this is relevant' ],
	[ 's:meta:s.channel', 'params.media.channel', '' ],
	[ 's:meta:s.pageName', 'n/a', 'MISSING MAPPING; WILL NEED TO UPDATE' ],
	[ 's:sc:rsid', 'params.analytics.reportSuite' ],
	[ 's:sc:tracking_server', 'params.analytics.trackingServer' ],
	[ 's:sp:hb_version', 'n/a', 'Probably implementation detail' ],
	[ 's:sp:player_name', 'n/a', 'Probably implementation detail' ],
	[ 's:sp:sdk', 'n/a', 'Probably implementation detail' ],
	[ 's:stream:type', 'params.media.contentType' ],
	[ 's:user:mid', 'n/a', 'Probably implementation detail' ],
	[ 's:user:mid', 'n/a', 'Probably implementation detail' ],

];

let metadataList = [];

params.forEach((item) => {
	// used to find the text preceding and after the first "." in uvp name
	const regex = new RegExp('^(\\w+)\\.?(.+)?');
	const firstKey = item[1].match(regex)[1]; // always exists
	const secondKey = item[1].match(regex)[2]; // only on some items
	// ^ is for handling keys that have "." character
	metadataList.push(
		new metadata(
			item[0], // legacy name
			item[1], // uvp name
			!secondKey ? [ firstKey ] : [ firstKey, secondKey ], // path
			item[2] ? item[2] : '' // notes
		)
	);
});

let uvpFormat = () => {
	let jsonData = JSON.parse(document.uvpForm.uvpText.value);
	let parentDiv = document.getElementById('uvpContent');
	let newTable = document.createElement('table');
	newTable.setAttribute('id', 'uvpTable');
	parentDiv.appendChild(newTable);
	let headerRow = newTable.insertRow(-1);
	let uvpHead = headerRow.insertCell(-1);
	let legacyHead = headerRow.insertCell(-1);
	let valueHead = headerRow.insertCell(-1);
	uvpHead.innerText = 'UVP';
	uvpHead.className += 'uvpHead';
	uvpHead.setAttribute('onClick', 'tableSort(0)');
	legacyHead.innerText = 'Legacy';
	legacyHead.className += 'legacyHead';
	legacyHead.setAttribute('onClick', 'tableSort(1)');
	valueHead.innerText = 'value';
	valueHead.className += 'valueHead';

	metadataList.forEach((item) => {
		let { uvpName, legacyName, path, notes } = item;

		let value = () => {
			// for different levels of nesting
			let firstLevel = jsonData[path[0]];
			let secondLevel = firstLevel ? jsonData[path[0]][path[1]] : undefined; // node really doesn't like it when i try to make it look for a property behind a non-existant key

			if (firstLevel) {
				if (secondLevel != undefined) {
					// some values can be 0, which is evaluates to false
					return secondLevel;
				} else {
					if (typeof firstLevel == 'string') {
						return firstLevel;
					} else return undefined;
					// ^ for second level nesting items that dont exist
				}
			} else return undefined;
			// ^ for first level nesting items that don't exist
		};

		let row = newTable.insertRow(-1);
		let uvpCell = row.insertCell(-1);
		let legacyCell = row.insertCell(-1);
		let valueCell = row.insertCell(-1);
		let noteSpan = document.createElement("div")
		uvpCell.innerText = uvpName;
		legacyCell.innerText = legacyName;
		valueCell.innerText = value();
		if (value() === undefined || value() === '') {
			row.classList += 'missing ';
		}
		if (notes.length) {
			valueCell.appendChild(noteSpan)
			noteSpan.classList += 'tooltipText'
			noteSpan.innerText = `${notes}`
		}
		row.classList += 'tooltip visibleRow '; // .visible is already used for something else
	});
};

let isProp = (str) => {return str.toLowerCase().includes('s.prop')}
let iseVar = (str) => {return str.toLowerCase().includes('evar')}

let tableSort = (n) => {
	// modified from https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore example

	var table,
		rows,
		switching,
		i,
		x,
		y,
		shouldSwitch,
		asc,
		switchcount = 0;
	table = document.getElementById('uvpTable');
	switching = true;
	asc = true; // change this value based on class
	while (switching) {
		switching = false;
		rows = table.rows;

		for (i = 1; i < rows.length - 1; i++) {
			shouldSwitch = false;
			x = rows[i].getElementsByTagName('td')[n].innerHTML;
			y = rows[i + 1].getElementsByTagName('td')[n].innerHTML;
			// enables sorting for eVar and props based on int value (e.g. 2 < 11)
			if ( (isProp(x) && isProp(y)) || ((iseVar(x) && iseVar(y)) ) ) {
				x = parseInt(x.match(/(?:s\.prop|eVar)(\d+)/)[1])
				y = parseInt(y.match(/(?:s\.prop|eVar)(\d+)/)[1])
			}
			if (asc) {
				if (x > y) {
					shouldSwitch = true;
					break;
				}
			} else {
				if (x < y) {
					shouldSwitch = true;
					break;
				}
			}
		}

		if (shouldSwitch) {
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
			switchcount++;
		} else {
			if (switchcount == 0 && asc) {
				asc = false;
				switching = true;
			}
		}
	}
};

let displayFilter = () => {
	let filterTerm = dqs('#filterText').value.toLowerCase();
	let regex = new RegExp(filterTerm);
	let table = dqs('#uvpTable');
	let rows = table.rows;
	let useRegex = dqs('#uvpRegex').checked;
	let invert = dqs('#uvpInvert').checked; 

	for (i = 1; i < rows.length; i++) {
		let cells = rows[i].getElementsByTagName('td');
		let match = false;
		rows[i].classList.remove('filteredOut');
		// ^ to unhide previously hidden stuff
		for (j = 0; j < cells.length; j++) {
			// can't use forEach on HTMLCollection
			let cellText = cells[j].innerText.toLowerCase();
			let condition = useRegex ? cellText.match(regex) : cellText.includes(filterTerm);

			if (condition) {
				match = true;
				break;
			}
		}
		if (!invert ? !match : match) {
			rows[i].classList.add('filteredOut');
		}
	}
};

let clearTable = () => {
	let table = dqs('#uvpTable');
	table.remove();
};
