document.addEventListener('DOMContentLoaded', (e) => {
	// tabHandling adds an event handler for clicking on a tab
	let tabHandling = (tabName, contentName) => {
		let tab = document.getElementById(tabName);
		tab.addEventListener('click', (e) => {
			let content = document.getElementById(contentName);
			// if the clicked tab is for a hidden section, the section is displayed and the other sections are hidden
			if (content.className == 'hidden') {
				makeTabInactive();
				hideVisible();
				content.className = 'visible';
				// indicates which tab is active visually
				tab.className = tab.className + ' active';
			}
		});
	};
	// Adds handling to each section
	tabHandling('analyticsTab', 'analyticsContent');
	tabHandling('runtimeTab', 'runtimeContent');
	tabHandling('lookupTab', 'lookupContent');
	tabHandling('uvpTab', 'uvpContent');

	// Finds elements with class of 'visible' and replaces class with 'hidden'
	let hideVisible = () => {
		let activeContent = document.getElementsByClassName('visible');
		for (i = 0; i < activeContent.length; i++) {
			activeContent[i].classList = 'hidden';
		}
	};
	// Sets the class of all tabs to the default state
	let makeTabInactive = () => {
		let allTabs = document.getElementsByClassName('tablinks');
		for (i = 0; i < allTabs.length; i++) {
			allTabs[i].classList = 'tablinks';
		}
	};
});
